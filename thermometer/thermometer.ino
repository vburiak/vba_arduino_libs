void setup() {
  // put your setup code here, to run once:
  byte seg_pub_1 = 10;
  byte seg_pub_2 = 11;
  byte seg_A = 3;
  byte seg_B = 4;
  byte seg_C = 5;
  byte seg_D = 6;
  byte seg_E = 7;
  byte seg_F = 8;
  byte seg_G = 9;
  byte seg_P = 2;
  byte seg = seg_G;
  bool PUB_LEVEL = LOW;
  bool SEG_LEVEL = HIGH;

  pinMode(seg_pub_1, OUTPUT);
  pinMode(seg_pub_2, OUTPUT);
  pinMode(seg, INPUT_PULLUP);
  digitalWrite(seg_pub_1, LOW);
  digitalWrite(seg_pub_2, LOW);
  delay(100);
  if (digitalRead(seg) == 1) {
    
    PUB_LEVEL = HIGH;
    SEG_LEVEL = LOW;
    digitalWrite(seg_pub_1, HIGH);
    digitalWrite(seg_pub_2, HIGH);
  }
  
  pinMode(seg, OUTPUT);
  digitalWrite(seg_pub_1, !SEG_LEVEL);
  digitalWrite(seg_pub_2, !SEG_LEVEL);

  digitalWrite(seg_pub_1, PUB_LEVEL);
  while(1) {
    digitalWrite(seg, SEG_LEVEL);
    delayMicroseconds(100);
    digitalWrite(seg, !SEG_LEVEL);
    delay(10);
  }

}

void loop() {
  // put your main code here, to run repeatedly:

}
