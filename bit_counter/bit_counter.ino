byte out_byte = 0;
int button_pressed = 0;
int pressed_time = 50;
int long_pressed_time = 2000;

void setup() {
  // put your setup code here, to run once:
  
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);

  pinMode(10, INPUT_PULLUP);

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(9, LOW);
  digitalWrite(8, LOW);
  digitalWrite(7, LOW);
  digitalWrite(6, LOW);
  digitalWrite(5, LOW);
  digitalWrite(4, LOW);
  digitalWrite(3, LOW);
  digitalWrite(2, LOW);
  delay(1);
  if (out_byte & 1) digitalWrite(9, HIGH);
  if (out_byte & 2) digitalWrite(8, HIGH);
  if (out_byte & 4) digitalWrite(7, HIGH);
  if (out_byte & 8) digitalWrite(6, HIGH);
  if (out_byte & 16) digitalWrite(5, HIGH);
  if (out_byte & 32) digitalWrite(4, HIGH);
  if (out_byte & 64) digitalWrite(3, HIGH);
  if (out_byte & 128) digitalWrite(2, HIGH);

  if(digitalRead(10) == 0) {
      if(button_pressed < long_pressed_time) button_pressed++;
      if(button_pressed >= long_pressed_time) {
        out_byte = 0;
      }
      
  } else {
    if(button_pressed >= long_pressed_time) {
      button_pressed = 0;
    }
    else if(button_pressed >= pressed_time) {
      out_byte++;
      button_pressed = 0;
    }
  }  
}
